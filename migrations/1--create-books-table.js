module.exports = async db => {
    await db.query('DROP TABLE IF EXISTS `books`');
    await db.query(`
        CREATE TABLE \`books\` (
            \`id\` int(11) NOT NULL AUTO_INCREMENT,
            \`title\` varchar(255) NOT NULL,
            \`author\` varchar(255) NOT NULL,
            PRIMARY KEY (\`id\`)
        ) COLLATE 'utf8_general_ci'
    `);

    await db.execute('INSERT INTO `books` SET `title` = ?, `author` = ?', [
        `Harry Potter and the Philosopher's Stone`,
        'J. K. Rowling'
    ]);
    await db.execute('INSERT INTO `books` SET `title` = ?, `author` = ?', [
        'Winnie-the-Pooh',
        'Book by A. A. Milne'
    ]);
};
