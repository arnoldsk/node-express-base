class Router {
    constructor() {
        this._controllers = {};
        this.controllers = {};
    }

    route(controllerAtMethod) {
        this.controllers[controllerAtMethod] = this.getController(controllerAtMethod);

        return this.execute.bind(this, controllerAtMethod);
    }

    getController(controllerName) {
        if (controllerName.indexOf('@') !== -1) {
            controllerName = controllerName.split('@')[0];
        }

        if (typeof this._controllers[controllerName] === 'undefined') {
            this._controllers[controllerName] = require(`controllers/${controllerName}`);
        }
        return this._controllers[controllerName];
    }

    execute(controllerAtMethod, req, res) {
        const [controllerName, methodName] = controllerAtMethod.split('@');

        if (typeof this.controllers[controllerAtMethod] === 'undefined') {
            throw new Error(`Incorrect controller or method for route "${req.route.path}" => "${controllerAtMethod}"`);
        }

        this.setResLocals(req, res, controllerAtMethod);

        const Controller = new this.controllers[controllerAtMethod](req, res)
        Controller[methodName](req, res);
    }

    setResLocals(req, res, controllerAtMethod) {
        const [controllerName, methodName] = controllerAtMethod.split('@');

        res.locals.config = require('config').app;
        res.locals.route = {
            fullName: controllerAtMethod.replace('@', '-'),
            path: req.path,
            controller: controllerName,
            method: methodName,
        };
    }

}

module.exports = new Router;
