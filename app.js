require('module-alias/register')
const path = require('path');
const express = require('express');
const app = express();
const Router = require('root/router');
const session = require('express-session');
const config = require('config');

// TODO
// - Make all the test files and stuff the same way php artisan:make:app(or something) would work
// -- Copy files from some obscure folder, check migration version - require that the version is 1(?)

/**
 * View and Middleware configuration
 */
// Global app config for views
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-ejs-layouts'));
// Body and JSON parsers
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// Migration manager
app.use(require('root/migrator'));
// Cookies and sessions, message flasher
app.use(require('cookie-parser')());
app.set('trust proxy', 1);
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: config.get('app.url').indexOf('https:') === 0 }
}))
app.use(require('express-flash')());

/**
 * Routes
 * TODO: move this somewhere - this is getting silly
 */
app.get('/', Router.route('home@index'));
app.get('/test', Router.route('home@test'));

app.get('/books', Router.route('books@index'));
app.post('/books/create', Router.route('books@createPost'));
app.get('/books/remove/:id', Router.route('books@remove'));

/**
 * NotFound handler
 */
app.use((req, res, next) => {
    Router.setResLocals(req, res, 'error@404');
    res.status(404).render('errors/404');
});

/**
 * Error handler
 */
app.use((err, req, res, next) => {
    Router.setResLocals(req, res, 'error@500');
    res.status(500).render('errors/500');
    console.error(err);
});

/**
 * Instantiate the app
 */
app.listen(config.get('port'), () => {
    console.log(`Listening to port ${config.get('port')}!`);
});
