# Node Express Base

*Base... Well, it started as a plain ExpressJS base but I guess it's more of a framework at this point.*

    ~$ git clone ...
    ~$ cp config/sample.json config/default.json
    ~$ npm i
    ~$ npm i -g nodemon
    ~$ nodemon


# Routing

Routes are defined in `app.js`. Few examples are already defined.

    app.get('/test', Router.route('home@test'));

First part is the usual express routing, second part requires custom router class that will call corresponding controller and it's method. If you've worked with Laravel, this will make sense to you.

Think of the first part as the controller path, which means you can have `users/auth/login@check`. This will evaluate to `controllers/users/auth/login.js` and call the method `check()`.

# Views

Templating language used is `ejs`. Nothing more to say - read Documentation on how it's used.

Views have two global objects: `config` and `route`. Config is anything that's placed in `config/default.js` under "app".
Route holds few useful parameters describing route - these can be seen in `router.js` file's `setResLocals()` method.

The `<body>` tag has route controller and full name as classes for easier targeting with CSS/JS.
More or less the same how Magento does this but not as great.

# Models

The models use `mysql2` module with promise wrapper.
From my past experiences using mysql with node - callbacks get messy really fast so I'd say using await approach is an absolute must!

Please look at mysql2 documentation or refer to `models/books.js` and `controllers/books.js` method for await usage.

# Migrations

By default disabled in `config/default.json`. How it works: before any request, migrator checks if migrations are enabled and if the version set in config file is higher than the one stored in database table "migrations".

For example, if the version in database is 1 but the config has 2, the migrator will call file `migrations/2.js`.
Since there's no fancy ORM module, the files are kind of ugly, especially multi-line queries. But eh, it works.

Just because plain numbers don't describe much, there are two ways to name the files: `{version}.js` or `{version}--{anything-goes-here}.js`

# Other

Everything else, e.g. module aliases, controller request validation and config usage, can be figured out just checking what module is used or the extended class code.

There's too much stuff to be covered now. I ain't making a documentation for every aspect of this.
