const config = require('config');
const mysql = require('mysql2/promise');

class Model {
    constructor() {
        if (new.target === Model) {
            throw new TypeError('Cannot construct Abstract instances directly');
        }
    }

    async getInstance() {
        if (typeof this._instance === 'undefined') {
            this._instance = await mysql.createConnection({
                host: config.get('db.host'),
                user: config.get('db.user'),
                password: config.get('db.password'),
                database: config.get('db.database'),
            });
        }

        return this._instance;
    }

    async hasTable(tableName) {
        const db = await this.getInstance();
        const [rows, fields] = await db.execute(`
            SELECT *
            FROM information_schema.tables
            WHERE table_schema = ?
                AND table_name = ?
            LIMIT 1
        `, [config.db.database, tableName]);

        return rows.length !== 0;
    }
}

module.exports = Model;
