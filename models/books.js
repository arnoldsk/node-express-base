const Model = require('models/model');

class Books extends Model {
    async all() {
        const db = await this.getInstance();
        const [rows, fields] = await db.query('SELECT * FROM `books` ORDER BY `title` ASC');

        return rows;
    }

    async create(data) {
        const db = await this.getInstance();
        const [rows, fields] = await db.execute('INSERT INTO `books` SET `title` = ?, `author` = ?', data);

        return rows.insertId;
    }

    async delete(value, field = 'id') {
        const db = await this.getInstance();
        const [rows, fields] = await db.execute('DELETE FROM `books` WHERE `' + field + '` = ?', [value]);

        return rows.affectedRows !== 0;
    }
}

module.exports = new Books;
