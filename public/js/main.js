/**
 * Main JavaScript class
 */
class Main {
    constructor() {
        this.setActiveMenuItemClass();
    }

    setActiveMenuItemClass() {
        const item = $(`header nav a[href="${ROUTE.path}"]`);
        if (item.length) {
            item.addClass('active');
        }
    }
}

/**
 * Instantiate on document.ready
 */
$(() => new Main());
