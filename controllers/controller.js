class Controller {
    constructor(req, res) {
        if (new.target === Controller) {
            throw new TypeError('Cannot construct Abstract instances directly');
        }

        this.req = req;
        this.res = res;
    }

    back() {
        const backUrl = this.req.header('Referer') || '/';
        this.res.redirect(backUrl);
    }

    validateBody(param, criteria) {
        return this.validate('body', param, criteria);
    }

    validateQuery(param, criteria) {
        return this.validate('query', param, criteria);
    }

    validate(type, param, criteria = []) {
        const value = this.req[type][param];
        const hasCriteria = c => criteria.indexOf(c) !== -1;

        // Validate existance
        if (typeof value === 'undefined') {
            return false;
        }

        // Validate length
        if (hasCriteria('required')) {
            if (typeof value === 'string') {
                if (hasCriteria('strict') && !value.trim().length) {
                    return false;
                } else if (!value.length) {
                    return false;
                }
            }

            if (typeof value === 'object') {
                if (hasCriteria('strict') && !Object.values(value).every(v => v.length)) {
                    return false;
                } else if (!Object.keys(value).length) {
                    return false;
                }
            }
        }

        return true;
    }
}

module.exports = Controller;
