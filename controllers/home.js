const Controller = require('controllers/controller');

class Home extends Controller {
    index(req, res) {
        res.render('home/index', { title: 'Home' });
    }

    test(req, res) {
        res.render('home/test', { title: 'Test' });
    }

    async books(req, res) {
        const Books = require('models/books');

        res.render('home/books', {
            title: 'All Books',
            books: await Books.all(),
        });
    }
}

module.exports = Home;
