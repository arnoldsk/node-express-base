const Controller = require('controllers/controller');

class Home extends Controller {
    async index(req, res) {
        const Books = require('models/books');

        res.render('books/index', {
            title: 'All Books',
            books: await Books.all(),
        });
    }

    async createPost(req, res) {
        if (
            this.validateBody('title', ['required', 'strict']) &&
            this.validateBody('author', ['required', 'strict'])
        ) {
            const books = require('models/books');
            await books.create([
                req.body.title.trim(),
                req.body.author.trim()
            ]);

            req.flash('info', 'Book created!');
        } else {
            req.flash('error', 'Cannot create the book.');
        }

        this.back();
    }

    async remove(req, res) {
        const bookId = parseInt(req.params.id);

        if (bookId > 0) {
            const books = require('models/books');
            const ok = await books.delete(req.params.id);

            req.flash('info', `Book ${ok ? 'is' : 'is not'} removed!`);
        } else {
            req.flash('error', 'Cannot remove the book.');
        }

        this.back();
    }
}

module.exports = Home;
