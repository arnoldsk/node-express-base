module.exports = async (res, req, next) => {
    const config = require('config');

    if (config.get('db.migrations.enabled')) {
        const model = require('models/model');
        const db = await model.prototype.getInstance();

        let userVersion = config.get('db.migrations.version');
        let version = 0;

        // Check if there's migrations table
        const hasMigrationsTable = await model.prototype.hasTable('migrations');
        if (!hasMigrationsTable) {
            await db.query('CREATE TABLE `migrations` (`version` int unsigned NOT NULL)');
        }

        // Check current version
        const [rows, fields] = await db.query('SELECT `version` FROM `migrations` LIMIT 1');
        if (rows.length) {
            version = rows[0].version;
        } else {
            await db.query('INSERT INTO `migrations` SET `version` = 0');
        }

        // Run migration files
        if (userVersion - version > 0) {
            const fs = require('fs');
            const path = require('path');
            const fns = fs.readdirSync(path.join(__dirname, `migrations`));

            for (var v = version + 1; v <= userVersion; v++) {
                for (const fn of fns) {
                    if (new RegExp(`${v}(--.*)?\.js`).test(fn)) {
                        await require(`root/migrations/${fn}`)(db);
                        console.log(`Migrated version ${v - 1} -> ${v}`);
                    }
                }
            }

            // Update version
            await db.execute('UPDATE `migrations` SET `version` = ?', [userVersion]);
        }
    }

    next();
};
